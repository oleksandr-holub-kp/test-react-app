import Head from "next/head";
import { FormSpy, Form, Field } from "react-final-form";

// from example
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// from example
const onSubmit = async (values) => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

const emailValidation = (value) =>
  value
    ? /^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@(?:[a-z0-9]+\-?[a-z0-9]+\.)+[a-z]{2,6}$/.test(
        value
      )
      ? undefined
      : "wrong format"
    : "required";

const passwordValidation = (value) =>
  value ? (value.length < 8 ? "min-length: 8" : undefined) : "required";

const LoginForm = () => (
  <div>
    <Form
      onSubmit={onSubmit}
      validateOnBlur
      render={({ handleSubmit, form }) => (
        <FormSpy subscription={{ valid: true }}>
          {(props) => (
            <form onSubmit={handleSubmit}>
              <div className="field-row">
                <Field name="email" validate={emailValidation}>
                  {({ input, meta }) => {
                    return (
                      <div className="filed-row__inner">
                        <input {...input} type="email" placeholder="Email" />
                        {meta.error && meta.touched && <p>{meta.error}</p>}
                      </div>
                    );
                  }}
                </Field>
              </div>

              <div className="field-row">
                <Field name="password" validate={passwordValidation}>
                  {({ input, meta }) => {
                    return (
                      <div className="filed-row__inner">
                        <input
                          {...input}
                          type="password"
                          placeholder="Password"
                        />
                        {meta.error && meta.touched && <p>{meta.error}</p>}
                      </div>
                    );
                  }}
                </Field>
              </div>

              <div className="field-row">
                <button type="submit" disabled={!props.valid}>
                  Submit
                </button>
              </div>
            </form>
          )}
        </FormSpy>
      )}
    />

    <style jsx>{`
      .field-row {
        width: 300px;
        margin-bottom: 20px;
        color: #282828;
      }

      .field-row input,
      .field-row button {
        height: 40px;
        padding: 0 15px;
        border-radius: 3px;
      }

      .field-row input {
        width: 100%;
        border: 1px solid #282828;
      }

      .field-row p {
        margin: 5px 0 0;
        font-size: 0.8em;
        color: #b0190e;
      }

      .field-row button {
        background-color: #282828;
        color: #fff;
        border: none;
        opacity: 1;
        transition: all 0.3s ease-in-out;
        cursor: pointer;
      }

      .field-row button:not(:disabled):hover {
        opacity: 0.9;
      }

      .field-row button:disabled {
        opacity: 0.6;
        cursor: not-allowed;
      }
    `}</style>
  </div>
);

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">REACT FINAL FORM</h1>

        <div className="grid">
          <LoginForm />
        </div>
      </main>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          width: 100%;
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 1.5rem;
        }

        .title {
          text-align: center;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
